package com.itm.dtos;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class DeviceDto {
    private Integer id;
    private String name;
    private String address;
    private Double maximumHourlyEnergyConsumption;
}
