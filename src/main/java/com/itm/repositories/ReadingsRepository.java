package com.itm.repositories;

import com.itm.entities.Reading;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface ReadingsRepository extends JpaRepository<Reading, Integer> {
    void deleteAllByDeviceId(Integer deviceId);
    boolean existsByDeviceId(Integer deviceId);
    List<Reading> findAllByDeviceId(Integer deviceId);
    boolean existsByDeviceIdAndTimestamp(Integer deviceId, Long timestamp);
    boolean existsByDeviceIdAndTimestampAndMeasuredEnergyConsumption(Integer deviceId, Long timestamp, Double measuredEnergyConsumption);
    Optional<Reading> findByDeviceIdAndTimestamp(Integer deviceId, Long timestamp);

    List<Reading> findAllByDeviceIdAndTimestampBetween(Integer deviceId, Long startTimestamp, Long endTimestamp);
}
