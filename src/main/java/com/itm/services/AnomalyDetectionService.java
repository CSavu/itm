package com.itm.services;

import com.itm.adapters.DeviceManagementServiceAdapter;
import com.itm.dtos.DeviceDto;
import com.itm.entities.Reading;
import com.itm.exceptions.InvalidPropertyException;
import com.itm.ws.DeviceAlertingHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.util.List;
import java.util.stream.Collectors;

import static com.itm.adapters.UserManagementServiceAdapter.AUTH_TOKEN;

@Service
@Slf4j
public class AnomalyDetectionService {

    @Autowired
    private DeviceManagementServiceAdapter dmsAdapter;
    @Autowired
    private ReadingsService readingsService;
    @Autowired
    private DeviceAlertingHandler deviceAlertingHandler;

    public void detectAnomalies(Reading reading) {
        DeviceDto device = dmsAdapter.getDevice(reading.getDeviceId(), AUTH_TOKEN);
        if (device == null) {
            log.error("Device with deviceId={} does not exist", reading.getDeviceId());
            throw new InvalidPropertyException();
        }

        long currentTimestamp = Instant.now().getEpochSecond();
        long oneHourAgo = currentTimestamp - 3600; // 3600 seconds in an hour

        List<Reading> deviceReadingsInTheLastHour = readingsService.getAllReadingsForDeviceId(device.getId()).stream()
                .filter(dr -> dr.getTimestamp() >= oneHourAgo)
                .collect(Collectors.toList());

        Double sumOfMeasuredValuesInTheLastHour = deviceReadingsInTheLastHour.stream().map(Reading::getMeasuredEnergyConsumption).reduce(0.0, Double::sum);

        if (sumOfMeasuredValuesInTheLastHour > device.getMaximumHourlyEnergyConsumption()) {
            log.warn("Device with deviceId={} has exceeded its maximum power consumption", device.getId());
            // Send alert to WebSocket topic
            String alertMessage = "Device with deviceId=" + device.getId() + " has exceeded its maximum power consumption with last hour's consumption=" + sumOfMeasuredValuesInTheLastHour;
            Integer userId = dmsAdapter.getUserIdForDeviceId(device.getId(), AUTH_TOKEN);
            deviceAlertingHandler.sendMessageToUser(userId.toString(), alertMessage);
        }
    }
}
