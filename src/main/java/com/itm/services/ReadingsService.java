package com.itm.services;

import com.itm.adapters.DeviceManagementServiceAdapter;
import com.itm.adapters.UserManagementServiceAdapter;
import com.itm.entities.Reading;
import com.itm.exceptions.InvalidPropertyException;
import com.itm.repositories.ReadingsRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import static com.itm.adapters.UserManagementServiceAdapter.AUTH_TOKEN;

@Service
@Slf4j
@Transactional
public class ReadingsService {

    private static final Long ALMOST_ONE_DAY_IN_SECONDS = 86399L;
    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private static final ZoneId EEST_ZONE_ID = ZoneId.of("Europe/Bucharest");

    @Autowired
    private ReadingsRepository readingsRepository;
    @Autowired
    private UserManagementServiceAdapter umsAdapter;
    @Autowired
    private DeviceManagementServiceAdapter dmsAdapter;

    public List<Reading> getAllReadings() {
        return readingsRepository.findAll();
    }

    public List<Reading> getAllReadingsForDeviceId(Integer deviceId) {
        return readingsRepository.findAllByDeviceId(deviceId);
    }

    public List<Reading> getAllReadingsForDeviceOnDate(Integer deviceId, String date) {
        LocalDate localDate = LocalDate.parse(date, DATE_FORMATTER);
        ZonedDateTime startOfDay = localDate.atStartOfDay(EEST_ZONE_ID);

        long startTimestamp = startOfDay.toInstant().getEpochSecond();
        long endTimestamp = startTimestamp + ALMOST_ONE_DAY_IN_SECONDS;

        return readingsRepository.findAllByDeviceIdAndTimestampBetween(deviceId, startTimestamp, endTimestamp);
    }

    public Reading upsertHourlyReading(Reading reading) {
        ZonedDateTime currentDateTime = ZonedDateTime.now(EEST_ZONE_ID);

        // Set the minutes and seconds to zero
        ZonedDateTime startOfHour = currentDateTime.withMinute(0).withSecond(0).withNano(0);

        // Set the minutes and seconds to 59 (end of hour
        ZonedDateTime endOfHour = currentDateTime.withMinute(59).withSecond(59).withNano(0);

        // Get the timestamp in seconds
        long startOfHourInSeconds = startOfHour.toInstant().toEpochMilli() / 1000L;
        long endOfHourInSeconds = endOfHour.toInstant().toEpochMilli() / 1000L;

        // If in current hour, create or update existing reading
        if (reading.getTimestamp() >= startOfHourInSeconds && reading.getTimestamp() <= endOfHourInSeconds) {
            // If reading with timestamp already exists
            if (readingsRepository.existsByDeviceIdAndTimestamp(reading.getDeviceId(), startOfHourInSeconds)) {
                Reading oldReading = readingsRepository.findByDeviceIdAndTimestamp(reading.getDeviceId(), startOfHourInSeconds).get();
                // Sum its energy consumption with that of the new reading
                oldReading.setMeasuredEnergyConsumption(oldReading.getMeasuredEnergyConsumption() + reading.getMeasuredEnergyConsumption());
                log.info(
                        "Updating reading with deviceId={}, timestamp={}, and measuredEnergyConsumption={}",
                        oldReading.getDeviceId(),
                        oldReading.getTimestamp(),
                        oldReading.getMeasuredEnergyConsumption()
                );
                return readingsRepository.save(oldReading);
            }
        }

        // If outside current hour or reading with timestamp does not exist, create new reading
        Reading newReading = new Reading(reading.getDeviceId(), startOfHourInSeconds, reading.getMeasuredEnergyConsumption());
        return createReading(newReading);
    }

    private Reading createReading(Reading reading) {
        if (AUTH_TOKEN == null) AUTH_TOKEN = umsAdapter.generateNewAuthToken();

        if (!dmsAdapter.doesDeviceExist(reading.getDeviceId(), AUTH_TOKEN)) {
            log.error("Device with deviceId={} does not exist", reading.getDeviceId());
            throw new InvalidPropertyException();
        }

        if (readingsRepository.existsByDeviceIdAndTimestampAndMeasuredEnergyConsumption(reading.getDeviceId(), reading.getTimestamp(), reading.getMeasuredEnergyConsumption())) {
            log.error(
                    "Reading with deviceId={}, timestamp={}, and measuredEnergyConsumption={} already exists",
                    reading.getDeviceId(),
                    reading.getTimestamp(),
                    reading.getMeasuredEnergyConsumption()
            );
            throw new InvalidPropertyException();
        }

        return readingsRepository.save(reading);
    }

    public boolean deleteAllReadingsWithDeviceId(Integer deviceId) {
        if (readingsRepository.existsByDeviceId(deviceId)) {
            try {
                readingsRepository.deleteAllByDeviceId(deviceId);
                log.info("Deleted all readings for deviceId={}", deviceId);
                return true;
            } catch (Exception e) {
                log.error(e.getMessage());
                return false;
            }
        }
        return false;
    }
}
