package com.itm.adapters;

import com.itm.clients.DeviceManagementServiceClient;
import com.itm.dtos.DeviceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DeviceManagementServiceAdapter {
    @Autowired
    private DeviceManagementServiceClient dmsClient;

    public boolean doesDeviceExist(Integer deviceId, String authToken) {
        return dmsClient.getById(deviceId, authToken).getId() != null;
    }

    public DeviceDto getDevice(Integer deviceId, String authToken) {
        return dmsClient.getById(deviceId, authToken);
    }

    public Integer getUserIdForDeviceId(Integer deviceId, String authToken) {
        return dmsClient.getUserIdForDevice(deviceId, authToken);
    }
}
