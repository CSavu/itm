package com.itm.clients;

import com.itm.dtos.AuthenticationResponseDto;
import com.itm.dtos.AuthorizationResponseDto;
import com.itm.requests.AuthenticationRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class UserManagementServiceClient {
    @Value("${itm.ums.client.authorization.verifyToken.url}")
    private String VERIFY_TOKEN_URL;
    @Value("${itm.ums.client.user.getById.url}")
    private String GET_USER_BY_ID_URL;
    @Value("${itm.ums.client.authentication.authenticate.url}")
    private String AUTHENTICATE_URL;
    @Value("${itm.ums.client.admin.username}")
    private String ADMIN_USERNAME;
    @Value("${itm.ums.client.admin.password}")
    private String ADMIN_PASSWORD;

    private final RestTemplate restTemplate;

    @Autowired
    public UserManagementServiceClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public AuthorizationResponseDto verifyToken(String token) {
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Bearer " + token);

            HttpEntity<Void> requestEntity = new HttpEntity<>(headers);

            ResponseEntity<AuthorizationResponseDto> responseEntity = restTemplate.postForEntity(
                    VERIFY_TOKEN_URL,
                    requestEntity,
                    AuthorizationResponseDto.class
            );

            return responseEntity.getBody();
        } catch (Exception e) {
            // Handle general exceptions
            e.printStackTrace();
            return null;
        }
    }

    public AuthenticationResponseDto authenticate() {
        try {
            HttpHeaders headers = new HttpHeaders();

            HttpEntity<AuthenticationRequest> requestEntity = new HttpEntity<>(new AuthenticationRequest(ADMIN_USERNAME, ADMIN_PASSWORD), headers);

            ResponseEntity<AuthenticationResponseDto> responseEntity = restTemplate.postForEntity(
                    AUTHENTICATE_URL,
                    requestEntity,
                    AuthenticationResponseDto.class
            );

            return responseEntity.getBody();
        } catch (Exception e) {
            // Handle general exceptions
            e.printStackTrace();
            return null;
        }
    }
}
