package com.itm.clients;

import com.itm.dtos.DeviceDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

@Component
public class DeviceManagementServiceClient {
    @Value("${itm.dms.client.device.getById.url}")
    private String GET_BY_ID_URL;
    @Value("${itm.dms.client.device.getUserIdForDevice.url}")
    private String GET_USER_ID_FOR_DEVICE_URL;

    private final RestTemplate restTemplate;

    @Autowired
    public DeviceManagementServiceClient(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public DeviceDto getById(Integer deviceId, String token) {
        try {
            String url = UriComponentsBuilder.fromUriString(GET_BY_ID_URL)
                    .queryParam("deviceId", deviceId)
                    .toUriString();

            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Bearer " + token);

            HttpEntity<Void> requestEntity = new HttpEntity<>(headers);

            ResponseEntity<DeviceDto> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    requestEntity,
                    DeviceDto.class
            );

            return responseEntity.getBody();
        } catch (Exception e) {
            // Handle general exceptions
            e.printStackTrace();
            return null;
        }
    }

    public Integer getUserIdForDevice(Integer deviceId, String token) {
        try {
            String url = UriComponentsBuilder.fromUriString(GET_USER_ID_FOR_DEVICE_URL)
                    .queryParam("deviceId", deviceId)
                    .toUriString();

            HttpHeaders headers = new HttpHeaders();
            headers.set("Authorization", "Bearer " + token);

            HttpEntity<Void> requestEntity = new HttpEntity<>(headers);

            ResponseEntity<Integer> responseEntity = restTemplate.exchange(
                    url,
                    HttpMethod.GET,
                    requestEntity,
                    Integer.class
            );

            return responseEntity.getBody();
        } catch (Exception e) {
            // Handle general exceptions
            e.printStackTrace();
            return null;
        }
    }
}