package com.itm.rabbitmq.consumers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.itm.entities.Reading;
import com.itm.services.AnomalyDetectionService;
import com.itm.services.ReadingsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class EnergyReadingsConsumer {
    @Autowired
    private ReadingsService readingsService;
    @Autowired
    private AnomalyDetectionService anomalyDetectionService;

    @RabbitListener(queues = {"${itm.rabbitmq.readings.queue}"})
    public void receiveReadings(String readingAsString) {
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            Reading reading = objectMapper.readValue(readingAsString, Reading.class);
            log.info(
                    "Reading received for deviceId={}, measuredValue={}, timestamp={}",
                    reading.getDeviceId(),
                    reading.getMeasuredEnergyConsumption(),
                    reading.getTimestamp()
            );
            readingsService.upsertHourlyReading(reading);
            anomalyDetectionService.detectAnomalies(reading);
        } catch (Exception e) {
            log.error("Error while reading and unmarshalling message from queue: {}", e.getMessage());
        }
    }
}
