package com.itm.rabbitmq.consumers;

import com.itm.services.ReadingsService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class DeviceUpdateConsumer {
    @Autowired
    private ReadingsService readingsService;

    @RabbitListener(queues = {"${itm.rabbitmq.device.delete.queue}"})
    public void receiveDeviceDeletionMessage(String deviceId) {
        log.info("Device deletion message received for deviceId={}", deviceId);
        readingsService.deleteAllReadingsWithDeviceId(Integer.parseInt(deviceId));
    }

}
