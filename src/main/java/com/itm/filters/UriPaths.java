package com.itm.filters;

public enum UriPaths {
    READINGS("/readings");

    private String path;

    UriPaths(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
