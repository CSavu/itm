package com.itm.filters;

import com.itm.adapters.DeviceManagementServiceAdapter;
import com.itm.dtos.AuthorizationResponseDto;
import com.itm.exceptions.UnauthorizedException;
import com.itm.jwt.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.itm.adapters.UserManagementServiceAdapter.AUTH_TOKEN;
import static com.itm.entities.Role.ADMIN;
import static com.itm.entities.Role.REGULAR_USER;
import static com.itm.filters.OperationPaths.GET_ALL_FOR_DEVICE_ON_DATE;
import static com.itm.filters.UriPaths.READINGS;

@Component
public class RoleFilter extends OncePerRequestFilter {
    @Autowired
    @Qualifier("handlerExceptionResolver")
    private HandlerExceptionResolver resolver;
    @Autowired
    private DeviceManagementServiceAdapter dmsAdapter;
    @Autowired
    private JwtUtils jwtUtils;

    @Override
    @Order(3)
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            String uri = request.getRequestURI();
            if (!uri.equals("/ws")) {
                AuthorizationResponseDto authorizationResponse = (AuthorizationResponseDto) request.getAttribute("authorizationResponse");
                String uriPrefix = uri.substring(4, uri.lastIndexOf('/'));
                String operation = request.getRequestURI().substring(uri.lastIndexOf('/'));

                String deviceIdAsString = request.getParameter("deviceId");

                Integer userIdForDeviceId = null;
                if (deviceIdAsString != null) {
                    userIdForDeviceId = dmsAdapter.getUserIdForDeviceId(Integer.parseInt(deviceIdAsString), AUTH_TOKEN);
                }

                if (deviceIdAsString != null && authorizationResponse != null) {
                    if (REGULAR_USER.equals(authorizationResponse.getRole()) && READINGS.getPath().equals(uriPrefix) && GET_ALL_FOR_DEVICE_ON_DATE.getPath().equals(operation) && (userIdForDeviceId == null || !userIdForDeviceId.equals(authorizationResponse.getUserId()))) {
                        throw new UnauthorizedException("You are not authorized to access this resource");
                    }
                }

                if (authorizationResponse == null || (!ADMIN.equals(authorizationResponse.getRole()) && READINGS.getPath().equals(uriPrefix) && !GET_ALL_FOR_DEVICE_ON_DATE.getPath().equals(operation))) {
                    throw  new UnauthorizedException("You are not authorized to access this resource");
                }
            }

            filterChain.doFilter(request, response);
        } catch (UnauthorizedException e) {
            logger.warn("Unauthorized request");
            resolver.resolveException(request, response, null, e);
        }
    }
}
