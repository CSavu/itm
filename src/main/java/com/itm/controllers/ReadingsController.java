package com.itm.controllers;

import com.itm.adapters.UserManagementServiceAdapter;
import com.itm.entities.Reading;
import com.itm.services.ReadingsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/readings")
public class ReadingsController {
    @Autowired
    private ReadingsService readingsService;
    @Autowired
    private UserManagementServiceAdapter userManagementServiceAdapter;

    @GetMapping("/getAll")
    public List<Reading> getAll() {
        return readingsService.getAllReadings();
    }

    @GetMapping("/getAllForDeviceOnDate")
    @ResponseBody
    public List<Reading> getAllReadingsForDeviceOnDate(@RequestParam Integer deviceId, @RequestParam String date) {
        return readingsService.getAllReadingsForDeviceOnDate(deviceId, date);
    }

    @DeleteMapping("/deleteAllByDeviceId")
    public boolean deleteAllByDeviceId(@RequestParam Integer deviceId) {
        return readingsService.deleteAllReadingsWithDeviceId(deviceId);
    }
}
