FROM openjdk:17.0.1
VOLUME /tmp
EXPOSE 8445
ARG JAR_FILE
COPY ${JAR_FILE} itm.jar
ENTRYPOINT ["java","-Dspring.profiles.active=docker","-jar","/itm.jar"]